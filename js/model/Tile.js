function Tile() {
  // Properties definition
  this.borderColor;
  this.backgroundColor;
  this.selectedBackgroundColor;
  this.hoverBackgroundColor;
  this.hoverBoolean; // temp save hover (old hoverBackgroundColor)
  this.selected; // boolean if is selected

  // Methods definition
  this.construct = function (borderColor, backgroundColor, selectedBackgroundColor, hoverBackgroundColor, hoverBoolean, selected) {
    this.borderColor = borderColor;
    this.backgroundColor = backgroundColor;
    this.selectedBackgroundColor = selectedBackgroundColor;
    this.hoverBackgroundColor = hoverBackgroundColor;
    this.hoverBoolean = hoverBoolean;
    this.selected = selected;
  };

  // getter and setter
  this.getBorderColor = function () { return this.borderColor; };
  this.setBorderColor = function (borderColor) { this.borderColor = borderColor; };

  this.getBackgroundColor = function () { return this.backgroundColor; };
  this.setBackgroundColor = function (backgroundColor) { this.backgroundColor = backgroundColor; };
  
  this.getSelectedBackgroundColor = function () { return this.selectedBackgroundColor; };
  this.setSelectedBackgroundColor = function (selectedBackgroundColor) { this.selectedBackgroundColor = selectedBackgroundColor; };
  
  this.getHoverBackgroundColor = function () { return this.hoverBackgroundColor; };
  this.setHoverBackgroundColorn = function (hoverBackgroundColor) { this.hoverBackgroundColor = hoverBackgroundColor; };
  
  this.getHoverBoolean = function () { return this.hoverBoolean; };
  this.setHoverBoolean = function (hoverBoolean) { this.hoverBoolean = hoverBoolean; };
  
  this.getSelected = function () { return this.selected; };
  this.setSelected = function (selected) { this.selected = selected; };
  
} // END Tile class
