function Pointer() {
  // Properties definition
  this.backgroundColor;
  this.selectedBackgroundColor; // for implement in a future
  this.tempColor; // if move then save the tile color
  this.positionX;
  this.positionY;

  // Methods definition
  this.construct = function (backgroundColor, selectedBackgroundColor, tempColor, positionX, positionY) {
    this.backgroundColor = backgroundColor;
    this.selectedBackgroundColor = selectedBackgroundColor;
    this.tempColor = tempColor;
    this.positionX = positionX;
    this.positionY = positionY;
  };

  // getter and setter
  this.getBackgroundColor = function () { return this.backgroundColor; };
  this.setBackgroundColor = function (backgroundColor) { this.backgroundColor = backgroundColor; };
  
  this.getSelectedBackgroundColor = function () { return this.selectedBackgroundColor; };
  this.setSelectedBackgroundColor = function (selectedBackgroundColor) { this.selectedBackgroundColor = selectedBackgroundColor; };
  
  this.getTempColor = function () { return this.tempColor; };
  this.setTempColor = function (tempColor) { this.tempColor = tempColor; };
  
  this.getPositionX = function () { return this.positionX; };
  this.setPositionX = function (positionX) { this.positionX = positionX; };
  
  this.getPositionY = function () { return this.positionY; };
  this.setPositionY = function (positionY) { this.positionY = positionY; };
  
} // END Pointer class
