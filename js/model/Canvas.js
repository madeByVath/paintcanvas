function Canvas() {
    // Properties definition
    this.size;
    this.tileSize;
    this.tileBorderSize;
    this.tiles = [];

    // Methods definition
    this.construct = function (size, tileSize, tileBorderSize, borderColor, backgroundColor, selectedBackgroundColor, hoverBackgroundColor) {
        this.size = size;
        this.tileSize = tileSize;
        this.tileBorderSize = tileBorderSize;
        for (var i = 0; i < this.size; i++) {
            var row = [];
            for (var j = 0; j < this.size; j++) {
                var tile = new Tile();
                tile.construct(borderColor, backgroundColor, selectedBackgroundColor, hoverBackgroundColor, "false");
                row.push(tile);
            }
            this.tiles.push(row);
        }
    };

    // getter and setter
    this.getSize = function () { return this.size; };
    this.setSize = function (size) { this.size = size; };

    this.getTileSize = function () { return this.tileSize; };
    this.setTileSize = function (tileSize) { this.tileSize = tileSize; };

    this.getTileBorderSize = function () { return this.tileBorderSize; };
    this.setTileBorderSize = function (tileBorderSize) { this.tileBorderSize = tileBorderSize; };

    this.getTiles = function () { return this.tiles; };
    this.setTiles = function (tiles, position) { this.tile[position] = tiles; };

} // END Canvas class
