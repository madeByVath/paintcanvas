(function () {
    var app = angular.module('CanvasApp');
    app.controller('CanvasController', ['$scope', '$window', function ($scope, $window) {

        // Controller properties
        // Scope variables
        $scope.action = "firstView"; // multi flag for show or hidden views
        $scope.backgroundColor = "black";
        $scope.canvasSize = 4;// how much tiles NxN
        $scope.tileSize = 100;// size of every tile
        $scope.tileBorderSize = 2;
        $scope.tileBorderColor = "#789456";
        $scope.pointerBackgroundColor = "#123123"; // special tile for move and select tiles
        $scope.canvas; // Object main canvas
        $scope.canvas2; // Object canvas for save data
        $scope.brushColor = "blue"; // active color
        $scope.brushTool = "PaintCell"; // active tool
        $scope.selectedBackgroundColor = "grey"; // for tile
        $scope.hoverBackgroundColor = "#0AABA1"; // or tile
        $scope.yourcolor = "#123123"; // Used in make your color
        $scope.pointer; // Object pointer
        $scope.hoverColorTemp = ""; // for hover 

        /**
         * @name: backgroundColorChoose
         * @date: 27/01/2017
         * @version: 1
         * @author: Christian Vath
         * @description: The user choose an color and take this color for work
         * @params {none}
         * @return: none
         */
        this.backgroundColorChoose = function (brushColor) {
            $scope.brushColor = brushColor;
        };

        /**
         * @name: function showmenu
         * @date: 27/01/2017
         * @version: 1
         * @author: Christian Vath
         * @description: show menu or hidde menu
         * @params {none}
         * @return: none
         */
        $scope.showMenu = function () {
            $scope.showMe = !$scope.showMe;
        };

        /**
         * @name: function showmenu2
         * @date: 27/01/2017
         * @version: 1
         * @author: Christian Vath
         * @description: show menu2 or hidde menu2
         * @params {none}
         * @return: none
         */
        $scope.showMenu2 = function () {
            $scope.showMe2 = !$scope.showMe2;
        };

        /**
         * @name: function createCanvas
         * @date: 27/01/2017
         * @version: 1
         * @author: Christian Vath
         * @description: Create canvas with the user inputs
         * @params {none}
         * @return: none
         */
        this.createCanvas = function () {
            // Active second view and hidde first view
            $scope.action = "secondView";
            // create main canvas
            $scope.canvas = new Canvas();
            $scope.canvas.construct($scope.canvasSize, $scope.tileSize, $scope.tileBorderSize, $scope.tileBorderColor, $scope.backgroundColor, $scope.selectedBackgroundColor, $scope.hoverBackgroundColor);
            // create second canvas for save same data
            $scope.canvas2 = new Canvas(); // Empty
            $scope.canvas2.construct($scope.canvasSize, "", "", "", "", "", "");
            // crete pointer tile
            $scope.pointer = new Pointer(); //backgroundColor, selectedBackgroundColor, positionX, getPositionY()
            $scope.pointer.construct($scope.pointerBackgroundColor, "black", "", 0, 0);
            //save background color from tile
            $scope.pointer.setTempColor($scope.canvas.tiles[0][0].getBackgroundColor());
            //Put Pointer in tile00, start position
            $scope.canvas.tiles[0][0].setBackgroundColor($scope.pointer.getBackgroundColor());
        };


        /**
         * @name: function clickTile
         * @date: 27/01/2017
         * @version: 1
         * @author: Christian Vath
         * @description: if the user click in canvas functions
         * @params {none}
         * @return: none
         */
        this.clickTile = function (x, y) {
            // Test if are any tile is selected and was changed brushtool
            for (var i = 0; i < $scope.canvas.getSize(); i++) {
                for (var j = 0; j < $scope.canvas.getSize(); j++) {
                    // if was selected any tile and was changed tool then give back origin color
                    if($scope.canvas.tiles[i][j].getSelected() === "true" && $scope.brushTool !== "PaintGroup"){
                        //give back origin color
                        $scope.canvas.tiles[i][j].setBackgroundColor($scope.canvas2.tiles[i][j].getBackgroundColor());
                        //set selected to false
                        $scope.canvas.tiles[i][j].setSelected("false");
                        //clear canvas2 
                        $scope.canvas2.tiles[i][j].setBackgroundColor("");
                    }
                }
             }

            // What a tool is selected?
            switch ($scope.brushTool) {
                case "PaintCell":
                    // Paint a tile
                    $scope.canvas.tiles[x][y].setBackgroundColor($scope.brushColor);
                    break;
                case "PaintGroup":
                    for (var i = 0; i < $scope.canvas.getSize(); i++) {
                for (var j = 0; j < $scope.canvas.getSize(); j++) {
                    // if selected = true
                            if ($scope.canvas.tiles[i][j].getSelected() === "true") {
                                // Paint with selected color
                                $scope.canvas.tiles[i][j].setBackgroundColor($scope.brushColor);
                                //set selected to false
                                $scope.canvas.tiles[i][j].setSelected("false");
                                //clear canvas2
                                $scope.canvas2.tiles[i][j].setBackgroundColor("");
                            }
                        }
                    }
                    break;
                
                //Paint a colum
                case "PaintColum":
                    for (var i = 0; i < $scope.canvas.getSize(); i++) {
                        $scope.canvas.tiles[i][y].setBackgroundColor($scope.brushColor);
                    }
                    break;
                //Pain a row
                case "PaintRow":
                    for (var i = 0; i < $scope.canvas.getSize(); i++) {
                        $scope.canvas.tiles[x][i].setBackgroundColor($scope.brushColor);
                    }
                    break;
                //Clear a tile
                case "ClearCell":
                    $scope.canvas.tiles[x][y].setBackgroundColor("white");
                    break;
                //Paint a row and a colum
                case "PaintRowAndColum":
                    for (var i = 0; i < $scope.canvas.getSize(); i++) {
                        $scope.canvas.tiles[x][i].setBackgroundColor($scope.brushColor);
                        $scope.canvas.tiles[i][y].setBackgroundColor($scope.brushColor);
                    }
                    break;
                default:
                    break;
            } // END switch
        }; // END clickTile function
        
        
        
        /**
         * @name: function Clear canvas
         * @date: 27/01/2017
         * @version: 1
         * @author: Christian Vath
         * @description: clear all the canvas
         * @params {none}
         * @return: none
         */
        this.ClearCanvas = function () {
            for (var i = 0; i < $scope.canvas.getSize(); i++) {
                for (var j = 0; j < $scope.canvas.getSize(); j++) {
                    $scope.canvas.tiles[i][j].setBackgroundColor("white");
                }
            }
        };

        

    }]); // END Controller
    
    /**
    * @name: directive ngRightClick
    * @date: 27/01/2017
    * @version: 1
    * @author: Christian Vath
    * @description: directive for active the right click
    */
    // http://stackoverflow.com/questions/15731634/how-do-i-handle-right-click-events-in-angular-js
    app.directive('ngRightClick', function($parse) {
        return function(scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function(event) {
                scope.$apply(function() {
                    event.preventDefault();
                    fn(scope, {$event:event});
                });
            });
        };
    });

    /**
    * @name: template add-canvas-view-form
    * @date: 27/01/2017
    * @version: 1
    * @author: Christian Vath
    * @description: show first view, form for create a canvas
    */
    angular.module('CanvasApp').directive("addCanvasViewForm", function () {
        return {
            restrict: 'E', // type of directive
            templateUrl: "view/templates/add-canvas-view-form.html",
            controller: function () {
                // When the document is ready execute this code
            },
            controllerAs: 'addCanvasViewFormCtrl' // This is the alias
        };
    });
    
    /**
    * @name: template2 add-canvas-painter
    * @date: 27/01/2017
    * @version: 1
    * @author: Christian Vath
    * @description: show second view, canvas, tool menu...
    */
    angular.module('CanvasApp').directive("addCanvasPainter", function () {
        return {
            restrict: 'E', // type of directive
            templateUrl: "view/templates/add-canvas-painter.html",
            controller: function () {
                // When the document is ready execute this code
            },
            controllerAs: 'addCanvasPainterCtrl' // This is the alias
        };
    });

})(); // END Angula function
